---
layout: page
title: My Projects
permalink: /projects/
date: 2015-01-30 10:30:00 +0900
---

# Projects

**Research implementations:**

- [VINeM](http://adrem.uantwerpen.be/vinem) -- Visual Interactive Neighborhood Miner [User Guide](/projects/vinem/) - [code](https://gitlab.com/cartification/vinem)
- [Carti-Rank](http://adrem.uantwerpen.be/cartirank) -- Carti-Rank: Finding Subspace Clusters using Ranked Neighborhoods [code](https://gitlab.com/adrem/carti-rank)
- [Carti-Clon](http://adrem.uantwerpen.be/clon) -- CLON: Detecting Cluster Structures by Ordered Neighborhoods [code](https://gitlab.com/adrem/carti-clon)
- [BigFim](http://adrem.uantwerpen.be/bigfim) -- Frequent itemset miner for BigData. [code](https://gitlab.com/adrem/bigfim-sa)
- [Cartification](http://adrem.uantwerpen.be/cartification) -- Neighborhood transformation for high dimensional data [code](https://gitlab.com/adrem/carticlus)
- [DM-Common](https://gitlab.com/eminaksehirli/dm-common) -- Generic tools and algorithms that I use for data mining tasks.
- [DiCLENS](http://www.cs.umb.edu/~smimarog/diclens/) -- Divisive Clustering Ensemble Algorithm [implementation](http://www.cs.umb.edu/~smimarog/diclens/diclens.jar)
- DBSCAN_BV -- Time efficient implementation of DBSCAN for binary data

**Other projects**

- A bash script to generate PDF letters [here](https://github.com/eminaksehirli/lettergenerator)
- A simple [k-means implementation](https://gist.github.com/eminaksehirli/2fb6346b24ea16b6c121) that I use in [GNU Octave](http://www.gnu.org/software/octave/)
- Bahçeşehir University Master of Science Thesis latex style files [here](/code/BSUMSThesis.latex.style.tar.bz2)
- [ffcrawler](https://github.com/eminaksehirli/ffcrawler) -- A crawler for [friendfeed](https://friendfeed.com)
- [Sources for this website](https://gitlab.com/eminaksehirli/memin-web)

