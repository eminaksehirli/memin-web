<h1>VINeM - Visual Interactive Neighborhood Mining</h1>
<p>This manual will run you through some scenarios which explain the basics of the GUI.</p>
<h2>Intallation</h2>
<p>VINeM is provided as an executable <code>jar</code> file, therefore, it can be run without an installation on any computer that has a <a href="https://www.java.com/">Java Runtime Environment</a>. Similarly, removing the files is enough for uninstalling. Do not forget to clean the recent files list before deleting the application.</p>
<h2>Running</h2>
<p>In most operating systems you can use right-click menu on the jar file menu to run the application. Or, you can give the following command on the console:</p>
<p><code>java -jar vinem.jar</code>
Performance can be improved by allocating more memory. The following command gives 1 gigabyte of memory:
<code>java -jar -Xmx1g vinem.jar</code></p>
<h2>Selecting the file</h2>
<p>Program starts with a file selector. To investigate a new file, enter the path to the file, either by using the respective input field or the <code>Browse File</code> button. </p>
<p>Data should be in a delimited format, satisfying the following properties:
- each row represents an instance, except the header row if present,
- each column represents a feature, except the header column if present,
- all instances have the same number of features,
- there are no missing values,
- real values should be in the USA locale (use . as decimal separator)</p>
<p>If the rows have names, i.e. the first column contains the names of the objects, or the columns have names, i.e. the first row contains the names of the attributes, then select the respective checkboxes. Enter the delimiter to the delimiter field and click the button <code>Run with a new file</code> to start the analysis. </p>
<p><img alt="File Selection Window" src="figs/fileSelection.png" /></p>
<p>A history of files is provided as well. You can click on a button with the name of a file to re-open it. To remove a file from the history click <code>X</code> button next to the file name.</p>
<h2>Interface</h2>
<p>After the selection of the file, two main panels of VINeM are shown:
1. Visualisation of the neighborhoods as a <code>Neighborhood Matrix</code>. Each column represents an object while each row represents the neighborhood of an object. 
2. Control panel.</p>
<p><img alt="Neighborhood Matrix" src="figs/neighMatrix.png" /> <img alt="Control panel" src="figs/controlPanel.png" /></p>
<h2>Modifying the neighborhood matrix</h2>
<p>By default, <code>kNN</code>s of each individual dimension are shown as neighborhoods.</p>
<h3>Neighborhood parameters</h3>
<p>Neighborhood parameters can be updated using the <code>Neighborhood</code> panel. <code>k-Nearest-Neighborhood</code> and <code>radius-based neighborhood</code> are supported. Sliders can be used to update the neighborhood size <code>k</code> or the neighborhood radius <code>eps</code>. Hovering over these sliders shows a tool tip containing their actual value. These sliders can also be incremented/decremented by one using the mouse scroll wheel.</p>
<h3>Neighborhood order</h3>
<p><code>Dimension Order</code> affects the order of the objects in the visualisation panel. Both the rows and columns are sorted according to the selected attribute. For convenience, the objects are sorted according to the projected dimension that is being shown.</p>
<p>On the other hand, order of a neighborhood matrix does not have to be same with the order in the projected neighborhood. For example, neighborhood matrix of attribute 1 can be sorted according to the values in the attribute 2. To break the sync between the order and the distance measure, clear the checkbox <code>Sync with distance measure</code>.</p>
<h2>Selection</h2>
<p>Selected objects are colored green in the <code>Neighborhood Matrix</code>. Selecting objects can be done in a couple of ways:</p>
<ol>
<li>
<p>Using the <code>Neighborhood Matrix</code>: You can press down the left mouse button, drag over an area and release. This will trigger a selection. Depending on the state of the radio buttons at the bottom of the <code>Selection</code> part of the control panel this will either select the area (<code>Select</code>), intersect the previous selection with the area (<code>And</code>), or add the area to the previous selection (<code>Or</code>).</p>
</li>
<li>
<p>By using the list of object names in the <code>Selection</code> part of the control panel. This list is ordered according to the order slider and it is in sync with the selection in the visualisation panel, so any objects selected in this list are also selected in the visualisation panel. It supports standard list selection controls, e.g. ctrl+clicking an id will add/remove it to/from the current selection.</p>
</li>
</ol>
<p>Once a selection has been made, a dialog is shown containing some information about the selected objects. The size of the selection is shown at the top and the names of the selected objects are shown at the bottom. In the middle, there is a table containing three statistics of the selected objects in each dimension: </p>
<ol>
<li>Support</li>
<li>Standard Deviation</li>
<li>Median Absolute Deviation (calculated on position of the objects in a sorted list).</li>
</ol>
<p><img alt="Selection Info" src="figs/selectionInfo.png" /></p>
<h2>Clustering</h2>
<p>You can create a cluster containing the selected objects by using the <code>Cluster selected</code> button in the <code>Selection</code> panel. Clusters can be created in other ways, such as mining which is explained in its own section.</p>
<p>If there are any identified clusters, information about the cluster(s) are shown in <code>Clusters Info</code> dialog. The first column contains a checkbox which determines whether to visualise the cluster in <code>Neighborhood Matrix</code>, visible clusters are colored red. The other columns give information about the clusters:</p>
<ol>
<li>cluster id</li>
<li>size</li>
<li>number of dimensions</li>
<li>dimensions of the cluster</li>
<li>object ids in the cluster </li>
</ol>
<p><img alt="Cluster Info" src="figs/clusterInfo.png" /></p>
<p>You can also do some fine-tuning by first selecting the cluster(s) you want to change. Normal list selection controls apply, e.g. ctrl+click adds/removes to/from current selection. The following operations can be applied on clusters:
1. Add/remove the selected objects to/from the cluster. 
2. Delete cluster(s).
3. Select the objects in the selected cluster(s).
4. Remove all filtered objects from the selected cluster(s).
5. Save the cluster(s) into a file.</p>
<h2>Filtering</h2>
<p>Objects can be filtered out using the buttons in the <code>Filtering</code> part of the control panel. A selection of objects can be removed from the visualisation panel and the list of objects in <code>Selection</code> by clicking the <code>Filter out selected</code> button. The <code>Filter out non-selected</code> button removes the objects that are not selected. 
<code>Clear filtereds</code> button cancels the filter list, i.e., makes all objects visible again.</p>
<p>Filtering is an important iterative process for the analysis. Here are the steps of an example scenario:
- filter objects 1 2 3, 
- filter objects 7 8 9,
- undo last filtering, i.e., show object 7 8 9 again
- clear the filtering, 
- filter objects 4 5 6. 
All of the steps are <em>undoable</em>, that is, the objects which were filtered will be re-shown and the selection of objects will be reset to those objects.</p>
<h2>Distance Measures</h2>
<p>By default, VINeM creates neighborhood matrices of 1-dimensional projections for every dimension/attribute. It is possible create neighborhood matrices for any combination of dimensions. This is done by using the <code>Distance measures</code> panel. Select the type of distance measure and the dimensions you want to add, and then, click the <code>Add</code> button. Doing this adds it to a list which can be selected from using the combo box at the bottom. </p>
<p>The option that is selected in the combo box is your <strong>Selected Distance Measure</strong>. <code>Neighborhood Matrix</code> is shown for this measure. Note that, while the checkbox <code>Sync with distance measure</code> (in <code>Dimension Order</code>) is checked, the order and the distance will be in sync.</p>
<h3>Sorting non-univariate measures</h3>
<p>Order in the <code>Neighborhood Matrix</code> is important for visual analysis. Unfortunately, <code>Dimension Order</code> can only be used for univariate, i.e. 1-dimensional, distance measures. It is possible to use a partial sorting for the non-univariate distance measures. <strong>Right clicking</strong> on a column in <code>Neighborhood Matrix</code> selects the corresponding object as reference and sorts all of the objects according to their distances from that object. This is very useful to identify local clusters.</p>
<h2>Mining for clusters</h2>
<p>Two subspace clustering algorithms are provided in <code>Mining</code> panel:</p>
<ol>
<li><code>Fast Miner</code> finds the objects that form a cluster in multiple dimensions. It finds all of the clusters that are larger than the given size (<code>Min-Length</code>). It is very fast because it exploits the order in neighborhood database. As a trade-off it only works on the 1-dimensional neighborhood databases.</li>
<li><code>Sampling Miner</code> employs a Monte-Carlo process to find a sampling of all of the cluster structures. It takes minimum support and number of itemsets as parameters. It is provided for finding the clusters in non-univariate dimensions, while satisfying the interactivity time constraints. Its details are explained in <a href="http://dx.doi.org/10.1109/ICDM.2013.146">this paper</a>. </li>
</ol>
<p>The clusters that are found by the miners are added to the cluster list so that they can be further refined. Also, both of the miners can be run only on the selected objects by clicking the <code>Mine Selected</code> button.</p>
<h2>Mining for related dimensions</h2>
<p><code>Find related dims</code> in <code>Mining</code> panel finds the related dimensions. It finds a fixed number of object sets of size 5 that frequently occur in the individual neighborhood databases. Supports of these object sets in other dimensions are calculated as the similarity score between the pairs of dimensions. More info about this calculation can be found in the paper.</p>
<p><img alt="Related Dims" src="figs/relatedDims.png" /></p>
<p>The scores of dimension pairs are presented as a matrix in a separate window. Higher scores indicate more similarity. At the bottom of the window, there is a slider for selecting a threshold. Values in the matrix that are greater than the threshold are highlighted in green.</p>
<h2>Finding Outliers</h2>
<p>Outliers can be identified as the objects that are not in the neighborhoods of many objects, hence having low support. An outlier miner <code>Find Outliers</code> is provided under the <code>Mining</code> panel. It identifies the outliers by finding the objects that has a low support than <code>Min-Support</code> <em>in the selected measure</em> or <em>in each measure</em>, as well as finding the ones that has a lower support <em>globally</em>. Outliers are added to <code>Cluster List</code> so that the user can inspect and decide what to do with them, e.g, filter them out. </p>
<h2>Tools</h2>
<ul>
<li><code>Show Distribution</code> shows the supports of the objects in the <code>Neighborhood matrix</code>.</li>
<li><code>Save Matrix</code> saves the <code>Neighborhood Matrix</code> as an image file. </li>
</ul>