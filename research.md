---
layout: page
title: My Research
permalink: /research/
date: 2015-01-30 10:30:00 +0900
---

# Publications
- "[Identifying Cell Sector Clusters Using Massive Mobile Usage Records](https://doi.org/10.1109/WCNC45663.2020.9120809)" Zhe Chen and _Emin Aksehirli_ 2020 IEEE Wireless Communications and Networking Conference (WCNC), 2020, pp. 1-6.
- "[Predicting MRT Trips in Singapore by Creating a Mobility Behavior Model Based on GSM Data](https://doi.org/10.1109/ICDMW.2018.00098)" _Emin Aksehirli_ and Ying Li in 2018 IEEE International Conference on Data Mining Workshops (ICDMW), 2018, pp. 632-639.
- "[Finding Subspace Clusters using Ranked Neighborhoods](http://dx.doi.org/10.1109/ICDMW.2015.202)" _Emin Aksehirli_, Siegfried Nijssen, [Matthijs van Leeuwen](http://www.patternsthatmatter.org/), and [Bart Goethals][bart] in IEEE International Conference on Data Mining Workshop (ICDMW), 2015, pp.831-838, 14-17 Nov. 2015 [PDF](http://www.patternsthatmatter.org/publications/2015/finding_subspace_clusters_using_ranked_neighborhoods-aksehirli_nijssen_vanleeuwen_goethals.pdf) [Implementation](http://adrem.uantwerpen.be/cartirank)
- "[Finding Subspace Clusters using Local Neighborhoods](http://hdl.handle.net/10067/1283490151162165141)" _Emin Aksehirli_, PhD. Thesis, University of Antwerp, 2015
- "[Efficient Cluster Detection by Ordered Neighborhoods](https://dx.doi.org/10.1007/978-3-319-22729-0_2)" _Emin Aksehirli_, [Bart Goethals][bart], and Emmannuel Müller in 17th International Conference on Big Data Analytics and Knowledge Discovery - DaWaK 2015, Springer International Publishing, 2015, pp. 15-27.
 [PDF](http://adrem.uantwerpen.be/sites/adrem.uantwerpen.be/files/paper.pdf) [Implementation](http://adrem.uantwerpen.be/clon)
- "[Visual Interactive Neighborhood Mining on High Dimensional Data](http://adrem.uantwerpen.be/vinem)" _Emin Aksehirli_, [Bart Goethals][bart], and Emmannuel Müller in KDD 2015 Workshop on Interactive Data Exploration and Analytics - [IDEA 2015](http://poloclub.gatech.edu/idea2015/) [PDF](http://adrem.uantwerpen.be/sites/adrem.uantwerpen.be/files/vinem/paper.pdf) [Implementation](http://adrem.uantwerpen.be/vinem)
- "[Cartification: A Neighborhood Preserving Transformation for Mining High Dimensional Data](http://dx.doi.org/10.1109/ICDM.2013.146)" _Emin Aksehirli_, [Bart Goethals][bart], Emmannuel Müller, and [Jilles Vreeken](http://people.mmci.uni-saarland.de/~jilles/) in Data Mining, 2013. ICDM 2013. Thirteenth IEEE International Conference on, 2013 IEEE [PDF](http://www.adrem.uantwerpen.be/bibrem/pubs/cartification.pdf) [Implementation](http://adrem.uantwerpen.be/cartification)
- "[Frequent Itemset Mining for Big Data](http://www.adrem.uantwerpen.be/bibrem/pubs/bigfim.pdf)" Sandy Moens, _Emin Aksehirli_, and [Bart Goethals][bart] in [In SML: BigData 2013 Workshop on Scalable Machine Learning, 2013 IEEE](https://sites.google.com/site/bigdatasml/home). [PDF](http://www.adrem.uantwerpen.be/bibrem/pubs/bigfim.pdf) [Implementation](https://gitlab.com/adrem/bigfim)
- "[New Cluster Ensemble Algorithm with Automatic Cluster Number and New Pruning Technique for Fast Detection of Neighbors on Binary Data](https://www.dropbox.com/s/bxnkj04goxzn84c/tez.pdf)" _Emin Aksehirli_, MSc. Thesis,  T.C. Bahcesehir Universitesi, Istanbul 2011
- "[DICLENS: Divisive Clustering Ensemble with Automatic Cluster Number](http://dx.doi.org/10.1109/TCBB.2011.129)" Selim Mimaroglu, _Emin Aksehirli_ in Computational Biology and Bioinformatics, IEEE/ACM Transactions on , vol.9, no.2, pp.408-420, March-April 2012, [Implementation](http://www.cs.umb.edu/~smimarog/diclens/)
- "[Improving DBSCAN's Execution Time by Using a Pruning Technique on Bit Vectors](http://dx.doi.org/10.1016/j.patrec.2011.06.003)" Selim Mimaroglu, _Emin Aksehirli_ in Pattern Recognition Letters, Volume 32, Issue 13, 1 October 2011, Pages 1572-1580, ISSN 0167-8655. Elsevier
- "[Mining Frequent Item Sets Efficiently by Using Compression Techniques](http://world-comp.org/p2011/DMI2130.pdf)" Selim Mimaroglu, Cagri Cubukcu, _Emin Aksehirli_, Ertunc Erdil in International Conference on Data Mining (DMIN&#39;11), 2011, pp. 308-312 [PDF](http://world-comp.org/p2011/DMI2130.pdf)

[bart]:https://uantwerpen.be/bart-goethals
