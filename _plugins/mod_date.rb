module Jekyll
  module MyFilters
    def modification_date(input)
      if input != nil and File.exists? input
        File.mtime(input)
      else
        ""
      end
    end
  end
end

Liquid::Template.register_filter(Jekyll::MyFilters)
