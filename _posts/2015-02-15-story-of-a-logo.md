---
layout: post
title: "Story of a Logo"
categories: design
thumbnail: /res/img/logo/4color-square.svg
image: /res/img/logo/4color-square.svg
thumbnail-type: image/svg
description: "The short story behind the 4 color glider logo."
tags: 
 - logo
 - story
 - life
---

After I had created my previous web site, I was looking for a favicon. Then I decided to use the [hacker symbol](http://www.catb.org/hacker-emblem/):

![](/res/img/logo/glider.svg)

It is known as [glider](https://en.wikipedia.org/wiki/Glider_(Conway's_Life)) in [Conway's game of life](https://en.wikipedia.org/wiki/Conway%27s_Game_of_Life). I like it because the _game_ has very simple rules and it is good representation of the _computer logic_, while glider is a _life form_ which progresses as a neighborhood. And yes, I consider myself a hacker for a long time. Recently, _hacking_ and _the hacker culture_ has been commercially marketted, so now more people know that it's not just _cracking_ computers and accounts. But let me state what I understand from hacking with a quote from [Richard Stallman](https://stallman.org/articles/on-hacking.html): "[H]acking means exploring the limits of what is possible, in a spirit of playful cleverness." This _exploration_ involves questioning the status-quo and challenging the rules of both administrations and nature. I have more comments on hacking culture but that's for another post, now let's continue with the logo.

The template I used[^1] for the blog involves a logo. Apparently, new trend is to put a circular cropped photo of the author. Although it makes more sense than designing a logo or using a stock image, I don't want my fellow readers to feel being watched and judged by my stare. Thus, I needed a logo.

Glider completes its cycle in exactly 4 iterations. As a matter of fact, 4 allows a very special form in [pop art](https://duckduckgo.com/?q=warhol+4+colors&t=canonical&iax=1&ia=images). Since this web site will be a combination of art reviews and informatics, I though this would make a very nice logo:

![](/res/img/logo/4color-square.svg)

But, as expected, square form did not match the logo size. Then, I tried this:

![](/res/img/logo/4color-rect.svg)

Unfortunately, the colors did not blend well with the rest of the site. Therefore, I had to fall back to one of these _sliglthly_ more boring versions:

![](/res/img/logo/logo-grad3.png)
![](/res/img/logo/logo-grad5.png)


Although my original design couldn't make it to the logo of this web site, I am sure I can use it in the future. If you wanna play with it, the [inkscape](https://inkscape.org/en/) file is [here](/code/glider.svg).


[^1]:More like, I ripped from [rsms.me](http://rsms.me)
