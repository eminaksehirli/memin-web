---
layout: post
title:  "Upgrading the key"
categories: informatics
description: "I've recently updated my GPG key. I talk about handling the signatures for the new key."
tags: 
 - gpg 
 - pgp 
 - security 
 - key signing 
---
I have been using 2048 bit DSA encryption with just one set of key pairs, i.e., no subkeys. I was thinking that this set-up is more than enough for me. After my first key signing party in [FOSDEM 2014][FOSDEM2014], I realized that collecting signatures is not easy. So, I decided to improve my [encryption practices][pk_encryption] by using,

- subkeys for encryption and signing
- and a stronger encryption.

Using subkeys is a good and recommended practice. There is a great series of blog posts about the importance of it and how to create one [here][key_blog]. Since these posts are thorough, I will not go into the details of creating a key.
I was warned during the last FOSDEM about the vulnerability of 2048 bit keys and it seems the 4096 bit RSA keys are the new standard. Hence, I'm upgrading the encryption level too.

I found [this Apache guide][apache-guide] that explains how to upgrade a key. Please check it for signing and trusting your new key after creating it.

I don't want to lose my signatures after the upgrade. Also, I don't want to weaken the web of trust by removing my key. Therefore, I will re-sign the keys of people and ask them to sign my new key.
Unfortunately, some of the keys are not mutually signed, that is, I didn't sign keys of some people that signed my key and there are some people that didn't sign my key but I signed theirs.

Before continuing, let's synchronize our keyring with the servers with the command `gpg --refresh-keys`. 

It is easy to find the signatures on my key. To get the fingerprint of my key, I use the command `pgp -K`, which lists all the private keys in my keyring. If you have more than one key, you have to identify the one you are using. ID of my key is `90D39AD1`. Then, I can find out the signatures on my key with just one command on the console:


    $ gpg --list-sigs 90D39AD1

    pub   2048D/90D39AD1 2013-08-31 [expires: 2017-08-29]
    uid                  Emin Akşehirli <emin.aksehirli@qmail.com>
    sig 3        90D39AD1 2013-11-29  Emin Akşehirli <emin.aksehirli@qmail.com>
    sig 3        DAAADB4C 2013-11-29  John Doe <john.doe@gmail.com>
    sig          9AAA4E4B 2014-02-02  Will Dao <will.dao@securemail.com>
    ....


So, with some regex-fu, I'll be able to extract the ID's of the signatures on my key.

The harder part is to find the keys that I have signed. Probably there is a script for it but my 5 minute search came empty and I decided to write my own. I am kind of lucky because I know that my local keyring holds all of the keys that I have signed. So, the scripting idea is simple: scan the keys on the keyring and check whether I've signed them.

I need to contact two groups of people: People who signed my key and people whose keys I signed. But, I know that signing and managing keys is not the most fun thing to do and the _sign-back_ requests are favour asking, I'll try to send as few emails as possible. Therefore, I have 3 groups of people:

1. Mutually signed
2. I signed their key (they did not sign my key)
3. My key is signed by them (I did not sign theirs)

You can find the bash script I've written [here][bash-script]. It creates three files with the keys ID's in them:

1. /tmp/mutual.txt
2. /tmp/only_i.txt
3. /tmp/only_they.txt

Next step is to sign and send the keys. For the keys in group 1 and 2, I decided to use [caff][caff-man] which can be found in the `signing-party` package in Debian and Ubuntu. I modified the email message accordingly and use both my new and old keys to sign it. I referred to [this][caff-blog-1] and [this][caff-blog-2] blog posts to configure caff. If you like alternatives, [PIUS][pius] is another utility for signing keys.

I used the following configuration option to use both of my keys for signing the messages _[Edit: Please see PS-2 below]_:

    # Select this/these keys to sign with
    $CONFIG{'local-user'} = [ qw{8E490395F4361444 3209AAD690D39AD1} ]; 


In my case, there were only a few people in group 3. I was too lazy to write a script but that should be perfectly possible to write a script that extracts the email and name of the key owner, create personalized texts, sign it and send.

Since I will not ask anything back for the people in group 2, I will not disturb them. I will sign their keys with my new key and synch with the server.

--------

**PS-1:** After sending the keys, I got a reply from a fellow friend that he also changed his keys. He provided [a nice guide for checking the new key](http://lapo.it/pgp/transition1.txt), I wish I provided a similar guide.

**PS-2:** I got many replies stating that I didn't use the old key to sign the message. I haven't check it again but it's possible that this config is wrong. It's also possible that popular clients don't support or don't make it obvious multiple signatures.

[pk_encryption]:https://en.wikipedia.org/wiki/Public-key_cryptography
[key_blog]:http://spin.atomicobject.com/2013/11/24/secure-gpg-keys-guide/
[bash-script]:https://gist.github.com/eminaksehirli/6942929d3a2fd0a3dbdb
[caff-man]:http://manpages.ubuntu.com/manpages/hardy/man1/caff.1.html
[apache-guide]:https://www.apache.org/dev/key-transition.html
[pius]:http://www.phildev.net/pius/
[caff-blog-1]:http://mruiz.openminds.cl/blog/index.php/2009/12/03/signing-keys-with-caff/
[caff-blog-2]:http://bloc.eurion.net/archives/2010/gpg-key-signing-with-caff/
[FOSDEM2014]:https://archive.fosdem.org/2014/

