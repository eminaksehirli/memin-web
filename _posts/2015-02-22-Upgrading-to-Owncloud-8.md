---
layout: post
title: "Upgrading to Owncloud 8"
categories: informatics
description: "Upgrading to Owncloud 8 with encryption requires excessive amount of system resources. I wrote down the steps of mirroring the system to my local computer, applying the upgrade, and uploading it back."
tags: 
 - owncloud
 - upgrade
 - life
 - guide
---
I've tried to upgrade my owncloud installation from version 7 to version 8. The upgrades by [Softaculous](http://www.softaculous.com/softaculous/) have never been a problem but in this instance it took so long, it didn't even finished. I've tried to restart the update a few times but it stuck every time. During a few days after first the release, search engines did not give me any clue about the problem. But after a couple of days [this issue](https://github.com/owncloud/core/issues/14012) hits the first page. The cause of the problem is that all of the encryption keys should be updated (renamed) and this takes time. Apparently, power of the poor shared hosting machine that I use is not enough to complete in time limits. I've decided to do the update on my local computer and then redeploy the owncloud installation.

Here are the steps of what I did:

1. Install Apache, MySql, and PHP on my local computer, addressing the [requirements](http://doc.owncloud.org/server/8.0/admin_manual/installation/source_installation.html#prerequisites) for an owncloud install. 
2. Download owncloud data folder, I'll call this folder `data`.
3. Change the group ownership of `data` to `www-data`, give group read write permission to all of the sub-directories, and give execute permission for all (`a+x`) to its parent directories.
4. Download owncloud installation folder, which resides probably under `~/public_html/` or `/var/www/`, I'll call this folder `oc`. Put it under apache server root.
5. Get a dump of the MySql database on the server.
6. Create a schema and a user on the local MySql server and execute/import the dump.
7. Update the database and directory information in `oc/config/config.php` and also turn off maintenance mode.
8. Restore encryption backup files as described [here](https://github.com/owncloud/core/issues/14012#issuecomment-74214431).
9. Open the URL in browser to check whether the owncloud installation works, **do not** start the upgrade since you cannot monitor it.
10. Stop apache server.
11. Change time out value from 3600 to 0 (no timeout) in `oc/lib/base.php`.
11. Run upgrade script in `oc` by `php ./occ upgrade`.
12. Wait until it finishes, **which took around 12 hours and more than 7 gigs of memory in my case**.
13. Start apache server and check whether everything is OK.
14. Stop the server and revert the changes in `oc/config/config.php` and `oc/lib/base.php`.
15. Move `encryption_migration-...` directories to somewhere else.
15. Upload upgraded MySql schema, data directory, and installation directory back to server.

Installing the required system is easy on GNU/Linux systems. You can follow the guide [here](https://help.ubuntu.com/community/ApacheMySQLPHP). Note on version: I'm still using Ubuntu 12.04 and the default repo still has php version 5.3. Owncloud needs at least 5.4. I used [this PPA](https://launchpad.net/~ondrej/+archive/ubuntu/php5) which is mentioned in [this tutorial](http://www.barryodonovan.com/2012/05/22/ubuntu-12-04-precise-pangolin-and-php-5-4-again) to upgrade my php version. I'm installing LAMP stack only for this upgrade, therefore the version collisions are not a problem. But if you have other php projects on the computer, be careful. Probably, I'll remove the packages and repo with [`ppa-purge`](http://manpages.ubuntu.com/manpages/raring/man1/ppa-purge.1.html) after the upgrade.

Since I've made the local apache install only for the update, I've used the default virtual site and copied `oc` folder directly under `/var/www/`, well `/var/www/html/` in newer apache versions.

Although it isn't required, I've created an sql user with the original credentials and give privileges to an sql schema with the original name. These information can be found in `oc/config/config.php`. 

I've downloaded the `data` folder into my home directory and update the location of `data` folder in the config file. If you put the `data` folder under your home directory, you should also give the execution permission for all (`a+x`) to all of its parent folders. While we are editing the config file, turn off the maintenance mode by changing `'maintenance' => true,` to `maintenance' => false,`.

Restore the backed-up encryption folders as described [here](https://github.com/owncloud/core/issues/14012#issuecomment-74214431) and [here](https://github.com/owncloud/core/issues/14012#issuecomment-73678268).

I use owncloud to backup my [zotero](https://www.zotero.org/) folder, which is very generous about creating files and directories. Therefore, my files_encryption folder contains 88,065 items although it only totals to 43.9 MB. Since the update time is more about the number of files than the size, I expect the upgrade to take a while. After I checked that the owncloud installation is working, I stopped the apache server and use the console command to upgrade instead of using the web interface. [This comment](https://github.com/owncloud/core/issues/14012#issuecomment-73848247) suggest that we use the command `sudo -u www-data php /var/www/owncloud/occ upgrade` but I got a warning about the user from the script. Then I just used `php ./occ upgrade`.

My first try terminated prematurely because it took more than an hour. Then, I followed [this comment](https://github.com/owncloud/core/issues/14012#issuecomment-73902209) and updated `oc/lib/base.php` to change the time out value to 0 in the following three lines,

{% highlight php %}
<?php
//...
//try to set the maximum execution time to 60min
set_time_limit(3600);
@ini_set('max_execution_time', 3600);
@ini_set('max_input_time', 3600); 
{% endhighlight %}

to 

{% highlight php %}
<?php
//...
//try to set the maximum execution time to 60min
set_time_limit(0);
@ini_set('max_execution_time', 0);
@ini_set('max_input_time', 0); 
{% endhighlight %}


During the upgrade I've checked whether it's running by using `iotop`. And monitoring the size of the `files_encryption` directory gives an idea about the progress.

Here should be a big **WARNING**: Upgrade took around *12 hours* on a core i7 computer with SSD and during the upgrade script use more than *7 gigs of memory*. These are very demanding requirements for a middle class server and definitely impossible to have on a shared hosting. My laptop has 8 gigs of memory in total and still it used around 2 gigs of swap space. SSD prevented the freezes so that it could finish, but I don't think it was possible without the SSD.

## Uploading back

After ensuring that your local owncloud installation works fine, 

- revert the changes in `oc/config/config.php` to the settings on the server,
- revert the timeout limits in `oc/lib/base.php` to be good a netizen on your shared hosting,
- move the `encryption_migration_...` directories out of the `data` and its sub folders, I will delete them if everything works fine,
- make sure you have backup of everything,
- take a MySql dump of the local MySql table,
- upload the MySql dump to the remote server,
- upload the folders using rsync with the commands `rsync -avz --delete oc/ server:~/public_html/oc` and `rsync -avz --delete data/ server:~/data`. Note the slashes at the end of the source folders; if they are omitted rsync creates the new `oc` folder in the `oc` folder.

This is the easy but not trivial step. Especially, uploading the database is a little bit tedious. Also, it is very important to take backups of everything before you regret.

When you see that everything works fine, delete the temporary backups.


### A note about encryption
This update become problematic because I use encryption. Since Owncloud's encryption is not on the client side, it's possible that the people who has access to server can decrypt the files. Although it's not the most secure one, I prefer people to go the extra mile to see my files than leaving them in clear. Fortunately, this is the first time upgrade becomes a problem but normally it's seamless.

-----

This the log after installation of the required php packages. It may become handy:

~~~ 
apache2-mpm-worker will be removed
apache2-mpm-prefork (version 2.2.22-1ubuntu1.7) will be installed
libapache2-mod-php5 (version 5.3.10-1ubuntu3.16) will be installed
libmcrypt4 (version 2.5.8-3.1) will be installed
php5-gd (version 5.3.10-1ubuntu3.16) will be installed
php5-intl (version 5.3.10-1ubuntu3.16) will be installed
php5-mcrypt (version 5.3.5-0ubuntu1) will be installed
php5-mysql (version 5.3.10-1ubuntu3.16) will be installed
~~~
