---
layout: post
title:  "Too Cool to be Forgotten"
categories: comic
description: "Too Cool to be Forgotten is a nice comic book which tells the story of a 40+ man going back to his high school years."
thumbnail: /res/img/tctbf/001_TOOCOOL.jpg
image: /res/img/tctbf/001_TOOCOOL.jpg
tags: 
 - graphic novel
 - comic
 - Alex Robinson
 - time travel
 - Top Shelf
 - DRM-free
 - Harvey Award
comic:
 writer: Alex Robinson
 art: Alex Robinson
 publisher: Top Shelf
 comixology: https://www.comixology.eu/Too-Cool-to-Be-Forgotten/digital-comic/21487
---

In *Too Cool to be Forgotten*, [Alex Robinson][alex-wiki] ([#][alex-tag]) asks a kind of old but still a very interesting question "**What if you can go back to your younger self?**" and then, he gracefully answers it.
{% include post-image.html src="/res/img/tctbf/001_TOOCOOL.jpg" alt="Too Cool to be Forgotten Cover" class="left" %}

What is it like to live by knowing the future? Would you still do the things that you did? 
Think about your high school crush that you cannot talk to without stuttering. Could you be much more smoother around him/her if you know that s/he will be looking up to you in 15 years. Or *would* you ask him/her out if you know that you will find out on your second date that s/he is the most boring person on earth.
Would getting bullied hurt that much if you know you will do fine in a few years?

I like that the book is taking a close look on everyday life of a teenager while keeping the perspective of an adult. For example our education system, putting young people into a room and making them listen to boring lectures 7 to 8 hours a day, 5 times a week... It looks like what *things should be* because it is almost impossible to see how annoying to sit all day with all these hormones and energy inside.
While I was studying, I wasn't very happy about the education, but frankly, I wasn't happy about many things back then. And after graduating I never looked back to do something about it.
This book gave me a chance to rethink about it. Maybe the whole education is intended to be boring as a preparation to [the adult life][fall-youtube].

{% include post-image.html src="/res/img/tctbf/013_TOOCOOL.jpg" alt="Too Cool to be Forgotten page 13" class="right" %}

While I was studying, I used to see people from upper grades as grown ups, people who were in their 30's were adults and you would be old after 40. Now, I'm 30 something myself and I _have_ the slightest idea about being an adult. Sure, I'm being responsible, reliable etc. but I still don't feel like an adult, I just know that I have to *act* like one. Moreover, now I see people in their 50's as *late-young*s, let alone being old. I had a good time during my high school years but when I think about having this much information back then, knowing that the 3 (or 4) years is a *pinch* of a life time, I may have done somethings differently.


I find this is the most suitable comic as the first review of this blog. As a man who left his high school 15 years behind, I could relate to the story, it . It's a nice read. I recommend it.

It's a personal story from [Alex Robinson][alex-wiki] ([#][alex-tag]), a straight white man, so I do not expect much about the representation of women or other races. I don't remember any annoying themes, but there is nothing against the everyday problems either.

{% include post-image.html src="/res/img/tctbf/015_TOOCOOL.jpg" alt="Too Cool to be Forgotten page 15" class="left" %}
{% include post-image.html src="/res/img/tctbf/016_TOOCOOL.jpg" alt="Too Cool to be Forgotten page 16" class="right" %}

<div style="clear:left"></div>

[Too Cool to be Forgotten on Top Shelf Productions](http://www.topshelfcomix.com/catalog/too-cool-to-be-forgotten/565)

[Too Cool to be Forgotten on Comixology](https://www.comixology.eu/Too-Cool-to-Be-Forgotten/digital-comic/21487)

[alex-wiki]:https://en.wikipedia.org/wiki/Alex_Robinson
[alex-tag]:/tags/Alex%20Robinson
[fall-youtube]:https://www.youtube.com/watch?v=A-rEb0KuopI
