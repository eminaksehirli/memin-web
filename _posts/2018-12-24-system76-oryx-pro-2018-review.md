---
layout: post
title:  "System76 Oryx Pro 2018 Review - Part&nbsp;1"
categories: informatics
description: "I have recently bought a System76 Oryx Pro. I give a summary of my experience."
thumbnail: /res/img/oryx/oryx_setup_small.jpg
image: /res/img/oryx/oryx_setup_small.jpg
tags: 
 - system76 oryx pro
 - review
 - notebook
 - personal
---

# Summary
I have recently bought a [System76 Oryx Pro 2018](https://system76.com/laptops/oryx). I have been planning to buy one for more than a year and finally took a leap of fate and ordered one. I have been using it for more than 2 months now and I wanted to write a review to help people who are trying to decide.

**TL;DR**: It's a **supreme computer** even by only its hardware and probably one of the best choice if you are looking for a GNU/Linux laptop.

## Pros
- You can customize the hardware configuration
- High-resolution matte screen looks amazing
- Keyboard and touch pad feels good
- It's a great feeling that somebody else is taking care of the GNU/Linux compatibility of the hardware
- GNU/Linux works as it's supposed to work
- It's designed for tech usage
- Great customer service
- Default disk encryption

## Cons
- Speakers are a bit muffled
- Keyboard organization could be better
- It gets hot while playing games

<img src="/res/img/oryx/oryx_setup.jpg" alt="Setup screen of Pop_os!" />
# Motivation
I find choosing a computer and especially a notebook computer a very personal and an individual process, hence the name **PC** 😊; therefore, I would like to give some background information about my usage habits and my motivation, so that you can judge my decisions. I have been using Ubuntu as my main operating system since 2007. So, a _native_ GNU/Linux computer is my obviuos choice. My personal computer, which had previously been also my work computer, was a [Dell Latitude E6420](https://www.dell.com/en-us/work/shop/cty/dell-latitude-e6430-premier-laptop/spd/latitude-e6430). I chose that computer because it was supposed to be GNU/Linux-compatible, and I was thinking it *was* ... until I get my Oryx -- more on this later. And I am currently using a MacBook Pro Mid-2015 for work.

I am a full time developer, I work from office and sometimes from home. So, mobile-working, and hence the battery life, is not my first priority. I also realized that I use my personal computer mostly to play games or playing around with data mining or machine learning algorithms. Therefore, I prefer performance over mobility, which eliminates the models like [Gazelle](https://system76.com/laptops/gazelle), [Dell XPS series](https://www.dell.com/en-us/shop/dell-laptops/xps-15/spd/xps-15-9560-laptop) or [purism](https://puri.sm/products/librem-15/). However, I still want to be able to sit on a couch with my computer on top of my lap or use my computer at a conference, which eliminates the behemoths like [Serval](https://system76.com/laptops/serval).

After a long consideration period, partly because of money matters and partly because my latitude ~~was~~ is still working perfectly, I decided to buy a gaming-capable and linux-compatible notebook. Around the time my Latitude was about turn its 7th year, Oryx Pro 2018 came out; and after a while, I have decided that it was a sign to order one.

Of course I had my concerns:

- It's almost impossible to try or even touch Oryx Laptop because they are not in retail shops. This is pretty important since I am quite picky about the UX. I would forever be annoyed if, for example, the left-ctrl key is not at the corner, or I cannot directly use Function keys (F1 - F12).
- Oryx Pro is not cheap. It's frightening to blindly buy something this expensive online.
- I don't live in the US, so, I can get in trouble if it breaks down. Even if it's ccovered by the warranty, I had to pay a significant amount for shipment.
- I wasn't sure about the build quality. I don't mind to fix a few minor things and annoyances, I use GNU/Linux after all, but I don't want to deal with low quality on my everyday PC.
- The actual GNU/Linux support. Will the bluetooth work? Can I install any other distribution? Do external screens work?

At the end I bought the below configuration and I can gladly say that *none of my concerns became a real problem*.

### My config:
* Pop!_OS 18.04 LTS (64-bit) with full-disk encryption
* 8 GB GTX 1070 with 2048 CUDA Cores
* 15.6″ Matte HiDPI 4K HiDPI Display
* 4.1 GHz i7-8750H (2.2 up to 4.1 GHz – 9MB Cache – 6 Cores – 12 Threads)
* 32 GB Dual Channel DDR4 at 2666MHz (2× 16GB)
* 500 GB NVMe PCIe M.2 SSD
* No Additional M.2 Drive
* No Additional 2.5″ Drive
* United States Keyboard
* WiFi up to 867 Mbps + Bluetooth
* Normal Assembly Service


# Peripherals

## Keyboard
Both the keyboard and the touch pad are crucial; after all, they are the interface that we use to interact with the PCs. To give you a better idea, I went to a retail shop and tried a few popular notebooks to find the most similar one. Oryx's keyboard is similar to keyboards of the popular gaming notebooks, especially HP OMENs. Overall, the keyboard feels amazing, compared to a pre-2016 MBP keyboard, slighlty bit more resistant to push at first but softer afterwards. It's back-lit and there are a four special fn-buttons on the keyboard to control the keyboard back-light: 1. turn on/off 2. switch colors 3. increase and 4. decrease the light intensity. *Bonus!* Check this project to play with colors: [https://gitlab.com/wmww/kb_backlight](https://gitlab.com/wmww/kb_backlight)

Although I am happy with its current status, keyboard organization could be improved:
1. The arrow bottons do not have any ridges (bumps, indicators) and the layout of keys are very similar to neighboring keys. This makes it harder to align the fingers while looking at the screen.
    <video autoplay loop style="width:100%">
      <source src="/res/img/oryx/arrow_keys.mp4" type="video/mp4">
      <source src="/res/img/oryx/arrow_keys.ogm" type="video/ogg">
    Your browser does not support the video tag.
    </video> 
2. Other navigation keys, that are, page up, page down, home, and end, are far away from the arrows. As a person who prefer to use keyboard for navigation instead of the touch pad, this creates a minor annoyance. I know this is not a Oryx-spcific issue but both of the other computers I use solve this issue quite ellegantly, Latitude has all of these keys grouped and MBP uses modifier + arrow keys.
3. No led indicators for Caps Lock, Num Lock and Scroll Lock.
4. This is not directly keyboard related but Power Button could be improved. It is too large and feels loose or unaligned. Therefore, it's difficult to tell whether I can press it correctly. Sometimes, I have to press a few times with different pressure levels to start up the computer.

I solved 1st and 2nd points by using the num pad, which has a ridge on 5 and all the navigation buttons within reach. Unfortunately, not all the apps suppport num pad keys. For example, Super + 8 does not go to the upper workspace, or ctrl + page up/down do not change tab in Gnome applications. They work in Firefox though, which is enough for now. Another note is that I have to use Gnome Tweaks to enable the "windows mode" for num pad to be able to select text with shift + num pad arrows.

For the 3rd point, I've found [Lock Keys](https://extensions.gnome.org/extension/36/lock-keys/) extension, which puts an indicator on the notification panel. This extension should be added to the Pop_Os! distribution as one of the default extensions. By the way, I use caps lock as a ctrl key. It can be configured in Gnome Tweak and releases the strain in left pinky finger. Try it if you haven't already.

## Touch pad

Touch pad is a pretty standard Synaptics device with two non-touch buttons for left and right click. It works exactly how it should, no problems and nothing extra ordinary -- even tough this may not look important, unfortunately I cannot say the same thing for my Latitude's touchpad which has become so twitchy that it's not usable anymore. As a reference, I played a [strategy game](https://www.gog.com/game/anno_1404_gold_edition) for a few hours without a mouse. After using MBP for a while, touch pad gestures are growing on me. I tried [libinput-gestures](https://github.com/bulletmark/libinput-gestures) for 3-finger-swipe workspace switching. It works well; however, because I've mapped three-finger touch to middle-mouse-button, everytime I use a three-finger gestures it fires a middle-button click, which may paste the selection. I find four-finger gestures a bit cumborsome but try them if you are ok with them.


# Photos for Comparison

Oryx Pro is slightly larger than the computers I have been using. Here are a few photos for you to compare.


<div class="gallery-box">
  <span style="width:70%">
    <div>
      <h4>MacBook® Pro 15" Mid-2015</h4>
      <p>Although they are both 15 inch, Oryx Pro is slightly larger than MacBook pro.</p>
    </div>
  </span>
  <span>
    <div>
      <a href="/res/img/oryx/vsMbpSize1.jpg" data-lightbox="compare" data-title="Oryx Pro vs Mac Book Pro Mid-2015 Size"><img alt="Oryx Pro vs Mac Book Pro Mid-2015 Size" src="/res/img/oryx/vsMbpSize1_th.jpg" data-lightbox="compare" style="margin-bottom:5px"/></a>
      <a href="/res/img/oryx/vsMbpSize2.jpg" data-lightbox="compare" data-title="Oryx Pro vs Mac Book Pro Mid-2015 Thickness"><img alt="Oryx Pro vs Mac Book Pro Mid-2015 Thickness" src="/res/img/oryx/vsMbpSize2_th.jpg" data-lightbox="compare"/></a>
    </div>
  </span>
</div>

<div class="gallery-box">
  <span style="width:70%">
    <div>
      <h4>MacBook® Pro 15" Mid-2018</h4>
      <p>MacBook® Pro 2018 versions are thinner than the previous versions. The availability of ports in Oryx Pro is obvious even from one-side photo.</p>
    </div>
  </span>
  <span>
    <a href="/res/img/oryx/vsMbp18Size1.jpg" data-lightbox="compare" data-title="Oryx Pro vs Mac Book Pro 2018 Size"><img alt="Oryx Pro vs Mac Book Pro 2018 Size" src="/res/img/oryx/vsMbp18Size1_th.jpg" data-lightbox="compare" style="margin-bottom:5px"/></a>
    <a href="/res/img/oryx/vsMbp18Size2.jpg" data-lightbox="compare" data-title="Oryx Pro vs Mac Book Pro 2018 Thickness"><img alt="Oryx Pro vs Mac Book Pro 2018 Thickness" src="/res/img/oryx/vsMbp18Size2_th.jpg" data-lightbox="compare"/></a>
  </span>
</div>

<div class="gallery-box">
  <span style="width:70%">
    <div>
      <h4>Dell Latitude® E6420</h4>
      <p>This is my older computer and Oryx is actually replacing this for me. Oryx is much thinner than the latitude and in terms of port availability it is only missing eSata and VGA, none of which is crucial.</p>
    </div>
  </span>
  <span>
    <a href="/res/img/oryx/vsLatitudeSize1.jpg" data-lightbox="compare" data-title="Oryx Pro vs Dell Latitude e6420 Size"><img alt="Oryx Pro vs Dell Latitude e6420 Size" src="/res/img/oryx/vsLatitudeSize1_th.jpg" data-lightbox="compare" style="margin-bottom:5px"/></a>
    <a href="/res/img/oryx/vsLatitudeSize2.jpg" data-lightbox="compare" data-title="Oryx Pro vs Dell Latitude e6420  Thickness"><img alt="Oryx Pro vs Dell Latitude e6420 Thickness" src="/res/img/oryx/vsLatitudeSize2_th.jpg" data-lightbox="compare"/></a>
  </span>
</div>

------------------

This post tries to compile some of my first impressions on Oryx Pro 2018. I'll try to write more about battery and performance in another post. Drop me a note if you are curious about anything specific.


