---
layout: page
title: About
permalink: /about/
date: 2015-01-30 10:30:00 +0900
---
# About Me
<div class="my-picture">
<img src="/res/img/emin.jpg" alt="Photo of Emin"/>
<p>
(Emin is the one with the red shirt.)</p>
</div>
I am currently working as a Data Scientist at [Booking.com](https://booking.com). Previously, I was at [Twitter](https://twitter.com/EminAksehirli) and [Data Spark](https://datasparkanalytics.com). I have defended my [PhD thesis](http://hdl.handle.net/10067/1283490151162165141) in October 2015. I conducted my academic research in [Advanced Database Research and Modeling](http://adrem.uantwerpen.be)  (ADReM) research group at [University of Antwerp](http://www.uantwerpen.be) under the supervision of [prof. Bart Goethals](https://www.uantwerpen.be/en/staff/bart-goethals/). My research area was data mining and I am currently working on *subspace clustering* and *instant interactive data mining*. More on [my research](/research).

I am interested in coding, causal inference, privacy, and cyber rights. Before getting involved in academic research, I was working as a software developer. One of my challenges during the PhD was to see whether I can apply software engineering rules to academic software. My research related software can be found in [projects](/projects) page along with my pet projects.

Although I am not a big fan of popular American culture, I am trying to keep up with [meme](http://en.wikipedia.org/wiki/Meme)s and trends. I have recently discovered the nice face of the comic book literature and I will try to write my thoughts on what I read. Be aware that I am just a concerned reader, not an expert of any kind.

## Contact

- m AT memin.tk
- My public GPG key is [3380 2171 FE81 1836 6A65 10E9 8E49 0395 F436 1444](https://keybase.io/emin).
- My linkedin: [eminaksehirli](https://linkedin.com/in/eminaksehirli)
- My Fediverse (Mastodon): [@emin@macaw.social](https://macaw.social/@emin)
- My twitter: [@EminAksehirli](https://twitter.com/EminAksehirli)
- My [GitLab](https://gitlab.com/u/eminaksehirli) and [GitHub](https://github.com/eminaksehirli)
- [My Google Scholar Citations page](http://scholar.google.com/citations?user=X1BxragAAAAJ&hl=en)
- [My DBLP page](http://www.informatik.uni-trier.de/~ley/pers/hd/a/Aksehirli:Emin.html)


## More from me

- Our GIF blog about research: [Research in Progress](http://researchinprogress.tumblr.com/)
- [My blog about free software](http://meminbuntu.blogspot.be/) in Turkish
- [My music bookmarks](http://heardontherdio.tumblr.com/)

<a name="site" />

### About this site

I publish almost everything I produce under [free licences](http://en.wikipedia.org/wiki/Free_license), i.e., [AGPL](http://en.wikipedia.org/wiki/Affero_General_Public_License) or [Creative Commons](http://en.wikipedia.org/wiki/Creative_Commons). **Except** the content that is obviously copyrighted by other parties, such as the screen shots, comic book pages or tarade marked logos. They are here probably under *fair use* but if anybody get upset let me know. ~~And lastly, template of this website is shamelessly ripped of from [rsms.me](http://rsms.me/).~~ I have changed the theme to [webjeda cards](https://webjeda.com/cards/) ([theme site](http://jekyllthemes.org/themes/webjeda-cards/)) on 2018-12-03.

Follow the blog:

- [rss]({{ '/feed.xml' |prepend: site.baseurl }})
- [atom]({{ '/atom.xml' |prepend: site.baseurl }})
- [rss for informatics]({{ '/feed_informatics.xml' |prepend: site.baseurl }})
- [my twitter](https://twitter.com/eminaksehirli)
